# Next.js E-commerce Platform

## Features

- **Store Layout:** A user-friendly main store layout that showcases products effectively.
- **Shopping Cart:** An interactive shopping cart for managing product selections before purchase.
- **User Registration:** A user account system for order tracking and faster checkout on future purchases.
- **Order Details Page:** Detailed views of order statuses and histories for registered users.
- **Admin Panel:** A secure admin panel for managing product listings, orders, and site content.
- **Cloud MongoDB:** Utilizes a cloud-hosted MongoDB instance for scalable and reliable data storage.
- **Stripe Payments:** Integrated Stripe payment gateway for secure and versatile payment options.
- **Email Notifications:** An email gateway integration for sending order confirmations and promotional materials.
- **Cloudinary Image Hosting:** Cloudinary is used for storing and serving product images, ensuring fast load times and optimal performance.

## Getting Started

### Prerequisites

Ensure you have the following installed and set up:
- Node.js (latest version)
- A MongoDB account and cloud instance
- Stripe account for handling payments
- Cloudinary account for image hosting
- An email service provider for sending emails

### Installation

1. **Clone the repository**

2. **Install dependencies**

```bash
yarn
```

3. **Set up environment variables**

Copy `.env.example` to `.env.local` and fill in your MongoDB URI, Stripe secret key, Cloudinary details, and email service configuration.

4. **Run the development server**

```bash
yarn dev
```